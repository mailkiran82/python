'''
__author__  ="Kiran Rajasekara
__email__   ="mailkiran82@gmail.com"
'''
from dask import dataframe as ddf
import pandas as pd
import json
import hashlib
import time
from difflib import SequenceMatcher
import difflib
import numpy as np
import multiprocessing
import os
import psutil
import mmh3
from datetime import datetime


''' set display option, only for viewing purpose '''
pd.set_option('display.max_columns', 256)
pd.set_option('display.max_colwidth', 512)
pd.set_option('display.precision', 15)
pd.set_option('display.width', 5000)

def print_df(ddf):
    for index, row in ddf.iterrows():
        print(index," -> ",row.tolist())

def my_timer(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print('TIME : {} : {} milliseconds'.format(method.__name__, (te-ts)*1000))
        return result
    return timed

def md5_helper(combinedstr):
    #m = hashlib.md5()
    #m.update(combinedstr.encode('utf-8'))
    #return m.hexdigest()
    m = mmh3.hash128(combinedstr.encode('utf-8'), signed=False)
    return m

@my_timer
def pre_check(df,side):
    print("{} Column Types : \n{}".format(side.upper(),df.dtypes))
    ''' list all the columns that contains NaN'''
    df_na_columns = df.columns[df.isna().any().compute()].tolist()
    if len(df_na_columns):
        print('PRE-CHECK : TIP : Found NaN in {} file, set default values for these columns \'{}\' in compareconfig'.format(side.upper(),df_na_columns))

def convert_to_dateformat(v, c, s):
    f = config['compareconfig'][s]['dateformat'][c]
    return datetime.strptime(str(v), f).strftime(f)

@my_timer
def pre_process(config, df,side):
    '''set the default values for NaN'''
    for k, v in config['compareconfig'][side]['fillna'].items():
        if v.lower() == "dateasobject" or v.lower() == "datetimeasobject":
            df[k] = df[k].fillna(config['compareconfig']['defaultvalues'][v]).astype(np.dtype(object))
        else:
            df[k] = df[k].fillna(config['compareconfig']['defaultvalues'][v]).astype(v)

    ''' list all the columns that contains NaN'''
    df_na_columns = df.columns[df.isna().any().compute()].tolist()
    if len(df_na_columns):
        print('ERROR : These columns have NaN in {} file, set default values for these columns \'{}\' in compareconfig'.format(side,df_na_columns))
        exit(1)

    ''' set string truncate as per compareconfig'''
    for k, v in config['compareconfig']['comparetolerance']['stringtruncate'].items():
        df[k] = df[k].apply(lambda x: x[:v], meta=(k, np.dtype(object)))

    '''set datatypes as per compareconfig'''
    for k, v in config['compareconfig'][side]['datatype'].items():
        if v.lower() == "datetime64":
            ''' datetime is overridden as object for faster processing.'''
            ''' changing format of the date use dateformat option'''
            df[k] = df[k].astype(np.dtype(object))
        else:
            df[k] = df[k].astype(v)

    '''set timeformat as per compareconfig'''
    for k, v in config['compareconfig'][side]['dateformat'].items():
        #df[k] = df[k].apply(lambda x: datetime.strptime(x, v).strftime(v), meta=(k, np.dtype(object)))
        df[k] = df[k].apply( convert_to_dateformat, args=(k,), s=side, meta=(k, np.dtype(object)))

    ''' set float precision as per compareconfig'''
    for k, v in config['compareconfig'][side]['floatprecision'].items():
        df[k] = df[k].apply(lambda x: round(x, v), meta=(k, 'f8'))

    '''set string values to lower'''
    for c in df.select_dtypes(include=[np.dtype(object)]).columns:
        df[c] = df[c].apply(lambda x: x.lower(), meta=(c, np.dtype(object)))

    ''' drop duplicate rows'''
    unique_df = df.drop_duplicates()
    return unique_df

def get_combinedstr(row):
    combinedstr = "~".join(row.astype(str)).lower()
    return md5_helper(combinedstr)

@my_timer
def compute_md5(df):
    ''' add a column with md5, to be used as key'''
    df['__MD5__'] = ''

    ''' join all the cells in the row'''
    df['__MD5__'] = df.apply( get_combinedstr, axis=1, meta=('__MD5__', 'int64'))

    md5count = df.__MD5__.nunique().compute()
    return md5count

def set_md5_index(df):
    df.set_index(keys=df.__MD5__, inplace=True)
    df.drop('__MD5__', axis=1, inplace=True)

def validate_md5(rowcount, md5count, side):
    if rowcount != md5count:
        print("ERROR : Duplicate MD5 found in {}. row count is {}, MD5 count is {}".format(side, rowcount, md5count))
        exit(1)
    else:
        print("DEBUG : {} MD5 is GOOD. row count is {}, MD5 count is {}".format(side, rowcount, md5count))

@my_timer
def remove_zero_balance_rows(config, df, side):
    ''' remove zero balance rows'''
    if config['compareconfig'][side]['removezerobalancerow'].lower() == "true":
        float_columns = df.select_dtypes(include=[np.float64]).columns.tolist()
        if len(float_columns) > 0:
            float_column_condition = [" (df."+i+" != 0.0) " for i in float_columns]
            condition_string = " | ".join(float_column_condition)
            #print("LOG : {} row count BEFORE remove_zero_balance_rows : {} ".format(side.upper(), len(df)))
            filtered_df = df[eval(condition_string)]
            #print("LOG : {} row count AFTER remove_zero_balance_rows : {} ".format(side.upper(), len(filtered_df)))
    return filtered_df if len(float_columns) > 0 else df


@my_timer
def check_float_tolerance(l_df, r_df):
    if config['compareconfig']['mode']['hintfloattolerancefields'].lower() == "true":
        common_columns =list(set(l_df.select_dtypes(include=[np.float64]).columns)
                             & set(r_df.select_dtypes(include=[np.float64]).columns))
        print('PRE-CHECK : TIP : set tolerance values for comparision for these float columns \'{}\' in compareconfig'
              .format(common_columns))

@my_timer
def check_string_truncate(l_df, r_df):
    if config['compareconfig']['mode']['hintstringtruncatefields'].lower() == "true":
        common_columns = (set(l_df.select_dtypes(include=[np.dtype(object)]).columns)
                             & set(r_df.select_dtypes(include=[np.dtype(object)]).columns))

        print('PRE-CHECK : TIP : set truncate values for these string columns \'{}\' in compareconfig'
              .format(common_columns))

        s_truncate = config['compareconfig']['comparetolerance']['stringtruncate']

        column_count = 0
        for c in common_columns.difference(set(s_truncate.keys())):
            column_count += 1
            ''' length check'''
            #''' try on sample data for faster processing'''
            #l_data_size = len(l_df)
            #r_data_size = len(r_df)
            #data_size = l_data_size if l_data_size > r_data_size else r_data_size
            #sample_size = 1.0 if data_size < 10000 else (10000/data_size)

            #print(" DEBUG : {} : {} : Verifying column {} for string tolerance with sample size {} ( <= 10,000 rows)".format(time.time(), column_count , c, sample_size))

            #l_sample_data = l_df[c].sample(frac=sample_size).compute()
            #r_sample_data = r_df[c].sample(frac=sample_size).compute()

            #l_max_len = len(max(l_sample_data, key=len))
            #r_max_len = len(max(r_sample_data, key=len))


            #''' try converting dask series -> pandas series for faster processing'''
            #t_l_se = l_df[c].unique().compute()
            #t_r_se = r_df[c].unique().compute()
            #l_max_len = len(max(t_l_se, key=len))
            #r_max_len = len(max(t_r_se, key=len))

            #print(" PRE-CHECK : DEBUG : {} : {} : Verifying column {} for string truncate.".format(time.time(), column_count , c))
            l_max_len = l_df[c].apply(lambda x: len(str(x)), meta=('x', 'int64')).max().compute()
            r_max_len = r_df[c].apply(lambda x: len(str(x)), meta=('x', 'int64')).max().compute()

            len_check = True if l_max_len == r_max_len else False

            if len_check == False:
                print(' PRE-CHECK : WARNING : set truncate values for this string column \'{}\' in compareconfig'.format(c))


@my_timer
def check_string_tolerance(l_df, r_df):
    if config['compareconfig']['mode']['hintstringtolerancefields'].lower() == "true":
        common_columns = (set(l_df.select_dtypes(include=[np.dtype(object)]).columns)
                             & set(r_df.select_dtypes(include=[np.dtype(object)]).columns))

        print('PRE-CHECK : TIP : set tolerance values for comparision for these string columns \'{}\' in compareconfig'
              .format(common_columns))

        s_tolerance = config['compareconfig']['comparetolerance']['stringtolerance']

        column_count = 0
        for c in common_columns.difference(set(s_tolerance.keys())):
            column_count += 1
            symmetric_difference = set(set(l_df[c].values.compute()) ^ set(r_df[c].values.compute()))

            if len(symmetric_difference):
                print('PRE-CHECK : WARNING : set tolerance values for comparision for this string column \'{}\' in compareconfig'
                .format(c))

def is_comparable(l_df, r_df):
    match_count = len([i for i, j in zip(l_df.columns.tolist(), r_df.columns.tolist()) if i == j])
    return True if match_count == len(l_df.columns.tolist()) else False

def add_md5_column(df, key_columns):
    ''' add a column with md5, to be used as key'''
    df['__MD5__'] = ''
    ''' join the cells to generate MD5'''
    for index, row in df.iterrows():
        combined_key_str = str('')
        for k, v in row.items():
            if k in key_columns:
                combined_key_str += str(v).lower().replace(' 00:00:00', '')
        df.loc[index, '__MD5__'] = md5_helper(combined_key_str)

@my_timer
def mark_comparable_rows(config, l_df, r_df):
    row_comparability = config['compareconfig']['comparetolerance']['rowcomparability'].lower()
    print("DEBUG : PARSE 2 : row_comparability : {} ".format(row_comparability))

    l_df['__COMBINED_ROW__'] = ''
    l_df['__IS_ROW_COMPARABLE__'] = 0

    r_df['__COMBINED_ROW__'] = ''
    r_df['__IS_ROW_COMPARABLE__'] = 0

    for index, row in l_df.iterrows():
        combined_full_str = "~".join(row.astype(str)).lower().replace(' 00:00:00', '')
        l_df.loc[index, '__COMBINED_ROW__'] = combined_full_str

    for index, row in r_df.iterrows():
        combined_full_str = "~".join(row.astype(str)).lower().replace(' 00:00:00', '')
        r_df.loc[index, '__COMBINED_ROW__'] = combined_full_str

    if row_comparability.lower() == 'low':
        row_comparability_count = 2
    elif row_comparability.lower() == 'medium':
        row_comparability_count = int(l_df.iloc[0]['__COMBINED_ROW__'].count('~')/2)
    else:
        row_comparability_count = int(l_df.iloc[0]['__COMBINED_ROW__'].count('~')/ 1.5)

    print("DEBUG : PARSE 2 : row_comparability_count : {} out of {}".format(row_comparability_count,l_df.iloc[0]['__COMBINED_ROW__'].count('~')))

    for o_index, o_row in l_df.iterrows():
        for i_index, i_row in r_df.iterrows():
            seq_match = SequenceMatcher(None, o_row['__COMBINED_ROW__'], i_row['__COMBINED_ROW__'])
            match = seq_match.find_longest_match(0, len(o_row['__COMBINED_ROW__']), 0, len(i_row['__COMBINED_ROW__']))
            a_start_pos = match[0]
            length = match[2]
            sub_str = str(o_row['__COMBINED_ROW__'])[a_start_pos:a_start_pos+length]
            if sub_str.count('~') >= row_comparability_count:
                l_df.loc[o_index, '__IS_ROW_COMPARABLE__'] = 1
                r_df.loc[i_index, '__IS_ROW_COMPARABLE__'] = 1

@my_timer
def find_key_value_columns(l_df, r_df, k_columns, v_columns):

    for c in l_df.columns.difference(['__MD5__','__COMBINED_ROW__','__IS_ROW_COMPARABLE__']):
        if l_df[c].dtype == np.float64:
            v_columns.append(c)
        elif l_df[c].dtype == np.dtype(object):
            ''' length check, if same length use it as key, else as value'''
            l_max_len = len(max(l_df[c], key=len))
            r_max_len = len(max(r_df[c], key=len))
            len_check = True if l_max_len == r_max_len else False
            if len_check == True:
                k_columns.append(c)
            else:
                v_columns.append(c)
        else:
            k_columns.append(c)

    print("DEBUG : PARSE 2 : key columns : {} ".format(k_columns))
    print("DEBUG : PARSE 2 : value columns : {} ".format(v_columns))

def my_compare_string(a, b, percentage):
    sa = len(a)
    sb = len(b)
    min = sa if sa < sb else sb
    max = sb if sb > sa else sa
    diff = max - min
    l = [li for li in difflib.ndiff(a.lower(), b.lower()) if li[0] != ' ']
    sl = len(l)
    cl = sl - (diff)
    p = int((1 - (cl / min)) * 100)
    return p >= int(percentage)

def generate_row_comparision_stmt(v_columns):
    s_tolerance = config['compareconfig']['comparetolerance']['stringtolerance']
    f_tolerance = config['compareconfig']['comparetolerance']['floattolerance']
    compare_stmt_conditions = []
    for c in v_columns:
        if c in s_tolerance.keys():
            compare_stmt_conditions.append(
                " my_compare_string(o_row." + c + ",i_row." + c + "," + str(s_tolerance[c]) + ") ")
        elif c in f_tolerance.keys():
            compare_stmt_conditions.append(" abs(o_row." + c + " - i_row." + c + ") <= "+str(f_tolerance[c])+" ")
        else:
            compare_stmt_conditions.append(" (o_row." + c + " == i_row." + c + ") ")

    compare_stmt = " and ".join(compare_stmt_conditions)
    return compare_stmt

def l_minus_r_union_r_minus_l(l_df, r_df, f_l_df, f_r_df):
    t_f_l_df = l_df[(~l_df.__MD5__.isin(r_df.__MD5__.values))].copy()
    t_f_r_df = r_df[(~r_df.__MD5__.isin(l_df.__MD5__.values))].copy()

    for index, row in t_f_l_df.iterrows():
        l_df.drop(l_df[l_df['__MD5__'] == row.__MD5__].index, inplace=True)

    for index, row in t_f_r_df.iterrows():
        r_df.drop(r_df[r_df['__MD5__'] == row.__MD5__].index, inplace=True)

    #t_f_l_df.drop(['__MD5__'], axis=1, inplace=True)
    #t_f_r_df.drop(['__MD5__'], axis=1, inplace=True)

    for index, row in t_f_l_df.iterrows():
        f_l_df.loc[index] = row
        #f_l_df.loc[index,'__FILTER__'] = 'from keycheck'

    for index, row in t_f_r_df.iterrows():
        f_r_df.loc[index] = row
        #f_r_df.loc[index, '__FILTER__'] = 'from keycheck'

def compare_single_row(compare_stmt, o_df, i_df, l_df, r_df, f_l_df, f_r_df):
    o_row = o_df.iloc[0]
    i_row = i_df.iloc[0]

    flag = eval(compare_stmt)

    if flag:
        ''' Match - delete the row from l_df and r_df'''
        l_df.drop(l_df[l_df.__MD5__ == o_row.__MD5__].index, inplace=True)
        r_df.drop(r_df[r_df.__MD5__ == i_row.__MD5__].index, inplace=True)

    else:
        ''' Not Match - copy to f_l_df and f_r_df'''
        for index, row in o_df.iterrows():
            #row.drop(['__MD5__'], inplace=True)
            f_l_df.loc[index] = row
            #f_l_df.loc[index, '__FILTER__'] = 'from valuecheck'

        for index, row in i_df.iterrows():
            #row.drop(['__MD5__'], inplace=True)
            f_r_df.loc[index] = row
            #f_r_df.loc[index, '__FILTER__'] = 'from valuecheck'

        ''' Not Match delete the row from l_df and r_df'''
        l_df.drop(l_df[l_df['__MD5__'] == o_row.__MD5__].index, inplace=True)
        r_df.drop(r_df[r_df['__MD5__'] == i_row.__MD5__].index, inplace=True)

def compare_multiple_row(compare_stmt, o_df, i_df, l_df, r_df, f_l_df, f_r_df):
    o_df['__MATCH_COUNT__'] = 0
    i_df['__MATCH_COUNT__'] = 0

    ''' compare lhs to rhs and get the row match count'''
    for o_index, o_row in o_df.iterrows():
        for i_index, i_row in i_df.iterrows():
            flag = eval(compare_stmt)
            if flag:
                o_row.__MATCH_COUNT__ += 1
                i_row.__MATCH_COUNT__ += 1
                o_df.loc[o_index, '__MATCH_COUNT__'] = o_row.__MATCH_COUNT__
                i_df.loc[i_index, '__MATCH_COUNT__'] = i_row.__MATCH_COUNT__

    ''' check for one to one match'''
    is_one_to_one_mapping = True
    for o_index, o_row in o_df.iterrows():
        if o_row.__MATCH_COUNT__ != 1:
            is_one_to_one_mapping = False
            break

    for i_index, i_row in i_df.iterrows():
        if i_row.__MATCH_COUNT__ != 1:
            is_one_to_one_mapping = False
            break

    if is_one_to_one_mapping:
        ''' Matched - remove from l_df and r_df'''
        for index, row in o_df.iterrows():
            l_df.drop(l_df[l_df['__MD5__'] == row.__MD5__].index, inplace=True)

        for index, row in i_df.iterrows():
            r_df.drop(r_df[r_df['__MD5__'] == row.__MD5__].index, inplace=True)
    else:
        ''' Ambigious - copy to f_l_df and f_r_df'''
        for index, row in o_df.iterrows():
            #row.drop(columns=['__MD5__', '__MATCH_COUNT__'], inplace=True)
            row.drop(columns=['__MATCH_COUNT__'], inplace=True)
            f_l_df.loc[index] = row
            #f_l_df.loc[index, '__FILTER__'] = 'from valuecheck - multiple ambigious rows'

        for index, row in i_df.iterrows():
            #row.drop(columns=['__MD5__','__MATCH_COUNT__'], inplace=True)
            row.drop(columns=['__MATCH_COUNT__'], inplace=True)
            f_r_df.loc[index] = row
            #f_r_df.loc[index, '__FILTER__'] = 'from valuecheck - multiple ambigious rows'

        ''' Ambigious - delete the rows from l_df and r_df'''
        for index, row in o_df.iterrows():
            l_df.drop(l_df[l_df['__MD5__'] == row.__MD5__].index, inplace=True)

        for index, row in i_df.iterrows():
            r_df.drop(r_df[r_df['__MD5__'] == row.__MD5__].index, inplace=True)

@my_timer
def compare_rows(l_df, r_df, f_l_df, f_r_df):
    key_columns = []
    value_columns = []

    ''' determine key columns and value columns'''
    find_key_value_columns(l_df, r_df, key_columns, value_columns)

    ''' generate md5 and set as column'''
    add_md5_column(l_df, key_columns)
    add_md5_column(r_df, key_columns)

    ''' copy lhs/rhs md5 not in rhs/lhs to final df'''
    l_minus_r_union_r_minus_l(l_df, r_df, f_l_df, f_r_df)

    compare_stmt = generate_row_comparision_stmt(value_columns)
    print(compare_stmt)

    key_union = list(set(l_df.__MD5__.tolist()) | set(r_df.__MD5__.tolist()))

    for k in key_union:
        o_df = l_df[l_df.__MD5__ == k].copy()
        i_df = r_df[r_df.__MD5__ == k].copy()

        if len(o_df) == 1 and len(i_df) == 1:
            ''' one row found for key'''
            compare_single_row(compare_stmt, o_df, i_df, l_df, r_df, f_l_df, f_r_df)
        else:
            ''' multiple row found for key'''
            compare_multiple_row(compare_stmt, o_df, i_df, l_df, r_df, f_l_df, f_r_df)

@my_timer
def dask_compute_preprocess(df):
    row_count = df.index.count().compute()
    return row_count

@my_timer
def write_diff_to_file(df1, df2):
    ''' write Unmatched File'''
    df1.to_csv('Unmatched_F1_LHS.csv', index=False)
    df2.to_csv('Unmatched_F1_RHS.csv', index=False)

    ''' write Diff file'''
    columns = [x for x in df1.columns.to_list() if x not in ['__MD5__']]
    merge_df = pd.DataFrame(columns=columns)
    key_union = list(set(df1.__MD5__.tolist()) | set(df2.__MD5__.tolist()))

    for k in key_union:
        p_l_df = df1[df1.__MD5__ == k].copy()
        p_r_df = df2[df2.__MD5__ == k].copy()

        if len(str(k)) > 0:
            p_l_df_i = len(p_l_df)
            p_r_df_i = len(p_r_df)

            loop = p_l_df_i if p_l_df_i < p_r_df_i else p_r_df_i

            l_counter = int(0)
            r_counter = int(0)
            for i in range(loop):
                l_row = p_l_df.iloc[i]
                r_row = p_r_df.iloc[i]
                m_row = {}
                m_row['__COMMENT__'] = "LHS And RHS"

                for key, value in l_row.items():
                    if key not in ('__MD5__'):
                        c_l = l_row[key]
                        c_r = r_row[key]

                        if c_l == c_r:
                            m_row[key] = c_l
                        else:
                            m_row[key] = str(c_l) + " != " + str(c_r)

                merge_df = merge_df.append(m_row, ignore_index=True)
                l_counter += 1
                r_counter += 1

            ''' write remaining lhs rows to merge_df'''
            for i in range(len(p_l_df) - l_counter):
                l_row = p_l_df.iloc[l_counter].copy()
                l_row.drop(['__MD5__'], inplace=True)
                l_row['__COMMENT__'] = "LHS Only"
                merge_df = merge_df.append(l_row, ignore_index=True)
                l_counter += 1

            ''' write remaining rhs rows to merge_df'''
            for i in range(len(p_r_df) - r_counter):
                r_row = p_r_df.iloc[l_counter].copy()
                r_row.drop(['__MD5__'], inplace=True)
                r_row['__COMMENT__'] = "RHS Only"
                merge_df = merge_df.append(r_row, ignore_index=True)
                r_counter += 1

        else:
            ''' wite non md5 rows to merge_df'''
            for k, row in p_l_df.iterrows():
                l_row = row.copy()
                l_row.drop(['__MD5__'], inplace=True)
                l_row['__COMMENT__'] = "LHS Only"
                merge_df = merge_df.append(l_row, ignore_index=True)

            for k, row in p_r_df.iterrows():
                r_row = row.copy()
                r_row.drop(['__MD5__'], inplace=True)
                r_row['__COMMENT__'] = "RHS Only"
                merge_df = merge_df.append(r_row, ignore_index=True)

        merge_df.to_csv('DIFF_LHS_RHS.csv', index=False)

if __name__ == "__main__":
    start_time = time.time()
    print("IN Main")

    '''read json file'''
    #with open('F1CompareConfig.json') as f:
    #with open('CompareConfig_10k.json') as f:
    with open('CompareConfig_1m.json') as f:
        config = json.load(f)

    ''' read lhs file '''
    #lhs_df = ddf.read_csv('F1_LHS.csv', low_memory=False)
    #lhs_df = ddf.read_csv('LHS_10k.csv', low_memory=False)
    #lhs_df = ddf.read_csv('LHS_100k.csv', low_memory=False)
    lhs_df = ddf.read_csv('LHS_1m.csv', low_memory=False)
    print(" INFO : LHS : row count = {} : ".format(len(lhs_df)))


    ''' read rhs file '''
    #rhs_df = ddf.read_csv('F1_RHS.csv', low_memory=False)
    #rhs_df = ddf.read_csv('RHS_10k.csv', low_memory=False)
    #rhs_df = ddf.read_csv('RHS_100k.csv', low_memory=False)
    rhs_df = ddf.read_csv('RHS_1m.csv', low_memory=False)
    print(" INFO : RHS : row count = {} : ".format(len(rhs_df)))

    ''' check number of cores for dask partition'''
    #print(" Core count:".format(psutil.cpu_count()))
    #npartition = psutil.cpu_count() / 2
    #print(" Dask Partition count:".format(npartition))
    #exit(0)

    print(" INFO : LHS : partition = {} : ".format(lhs_df.npartitions))
    print(" INFO : RHS : partition = {} : ".format(rhs_df.npartitions))

    runtype = config['compareconfig']['mode']['runtype'].lower()
    print(" INFO : runtype = {} ".format(runtype))

    #'''104857600 = 100MB'''
    #print(" INFO : LHS : Before repartition, partition = {} : ".format(lhs_df.npartitions))
    #lhs_df.repartition(npartitions=2 + lhs_df.memory_usage(deep=True).sum().compute() // 104857600)
    #print(" INFO : LHS : After repartition, partition = {} : ".format(lhs_df.npartitions))

    #print(" INFO : RHS : Before repartition, partition = {} : ".format(rhs_df.npartitions))
    #rhs_df.repartition(npartitions=2 + rhs_df.memory_usage(deep=True).sum().compute() // 104857600)
    #print(" INFO : RHS : After repartition, partition = {} : ".format(rhs_df.npartitions))

    ''' check if files are comparable, it expects the same column names and ordering in both lhs and rhs'''
    if is_comparable(lhs_df, rhs_df) == False:
       print("ERROR : Files are not comparable, check the headers. Requires the same column names and ordering in both lhs and rhs")
       exit(0)

    ''' Starting Phase 1'''
    print("INFO : PARSE 1")

    ''' check for NaN'''
    pre_check(lhs_df, 'lhs')
    pre_check(rhs_df, 'rhs')

    ''' check for float comparision tolerance'''
    check_float_tolerance(lhs_df, rhs_df)

    '''check for string comparision tolerance'''
    check_string_tolerance(lhs_df, rhs_df)

    '''check for string comparision tolerance'''
    check_string_truncate(lhs_df, rhs_df)

    if runtype.lower() == "scan":
        exit(0)

    ''' set NaN, datatype, dataframe, floatprecision'''
    #print("\nLHS Types Before : \n",lhs_df.dtypes)
    unique_lhs_df = pre_process(config, lhs_df, 'lhs')
    #print("LHS Types After : \n", unique_lhs_df.dtypes)

    #print("\nRHS Types Before : \n", rhs_df.dtypes)
    unique_rhs_df = pre_process(config, rhs_df, 'rhs')
    #print("RHS Types After : \n", unique_rhs_df.dtypes)


    '''remove rows with zero balance'''
    filtered_lhs_df = remove_zero_balance_rows(config, unique_lhs_df, 'lhs')

    filtered_rhs_df = remove_zero_balance_rows(config, unique_rhs_df, 'rhs')


    lhs_row_count = dask_compute_preprocess(filtered_lhs_df)
    print("LOG : LHS column types post pre_process : \n", filtered_lhs_df.dtypes)
    print("LOG : LHS row count post pre_process : {} ".format(lhs_row_count))
    rhs_row_count = dask_compute_preprocess(filtered_rhs_df)
    print("LOG : RHS column types post pre_process : \n", filtered_rhs_df.dtypes)
    print("LOG : RHS row count post pre_process : {} ".format(rhs_row_count))

    ''' generate md5 and set as index'''
    lhs_md5count = compute_md5(filtered_lhs_df)
    validate_md5(lhs_row_count, lhs_md5count, 'lhs')
    print("LOG : LHS Index __MD5__ Type : {} ".format(filtered_lhs_df.index.dtype))

    rhs_md5count = compute_md5(filtered_rhs_df)
    validate_md5(rhs_row_count, rhs_md5count, 'rhs')
    print("LOG : RHS Index __MD5__ Type : {} ".format(filtered_rhs_df.index.dtype))

    ''' get rows only in lhs'''
    lhs_only_df = filtered_lhs_df[(~(filtered_lhs_df.__MD5__.compute().isin(filtered_rhs_df.__MD5__.compute())))]

    ''' get rows only in rhs'''
    rhs_only_df = filtered_rhs_df[(~(filtered_rhs_df.__MD5__.compute().isin(filtered_lhs_df.__MD5__.compute())))]

    ''' convert dask.dataframe.core.DataFrame -> pandas.core.frame.DataFrame'''
    print("LOG : Converting Dask.dataframe to Pandas.dataframe... ")
    lhs_only_df = lhs_only_df.compute()
    rhs_only_df = rhs_only_df.compute()

    if len(lhs_only_df) == 0 & len(rhs_only_df) == 0:
        print('\n**************************************')
        print('*** RESULT : PASS : P1 : LHS = RHS ***')
        print('**************************************\n')
        print('Exiting... ')
        end_time = time.time()
        print('TIME : Total time taken : {} milliseconds '.format((end_time - start_time) * 1000))
        exit(0)

    ''' Starting Phase 2 - Row Logic'''
    print("INFO : PARSE 2 - Row Logic")

    set_md5_index(lhs_only_df)
    set_md5_index(rhs_only_df)

    print("\nlhs : \n", lhs_only_df.dtypes)
    print(lhs_only_df)
    print('lhs only row count : {}', len(lhs_only_df))
    #print(type(lhs_only_df))

    print("\nrhs : \n", rhs_only_df.dtypes)
    print(rhs_only_df)
    print('rhs only row count : {}', len(rhs_only_df))
    #print(type(rhs_only_df))

    mark_comparable_rows(config, lhs_only_df, rhs_only_df)


    ''' copy the non comparable rows to final df'''
    final_lhs_only_df = lhs_only_df[lhs_only_df['__IS_ROW_COMPARABLE__'] == 0].copy()
    final_rhs_only_df = rhs_only_df[rhs_only_df['__IS_ROW_COMPARABLE__'] == 0].copy()
    ''' drop columns'''
    final_lhs_only_df.drop(['__IS_ROW_COMPARABLE__','__COMBINED_ROW__'], axis=1, inplace=True)
    final_rhs_only_df.drop(['__IS_ROW_COMPARABLE__', '__COMBINED_ROW__'], axis=1, inplace=True)

    ''' add MD5'''
    final_lhs_only_df['__MD5__'] = ''
    final_rhs_only_df['__MD5__'] = ''
    '''add filter'''
    #final_lhs_only_df['__FILTER__']='from rowcomparability'
    #final_rhs_only_df['__FILTER__'] = 'from rowcomparability'

    '''drop the non comparable rows in working copy'''
    lhs_only_df.drop(lhs_only_df[lhs_only_df['__IS_ROW_COMPARABLE__'] == 0].index, inplace=True)
    rhs_only_df.drop(rhs_only_df[rhs_only_df['__IS_ROW_COMPARABLE__'] == 0].index, inplace=True)
    '''drop columns'''
    lhs_only_df.drop(['__IS_ROW_COMPARABLE__','__COMBINED_ROW__'], axis=1, inplace=True)
    rhs_only_df.drop(['__IS_ROW_COMPARABLE__', '__COMBINED_ROW__'], axis=1, inplace=True)

    ''' Starting Parse 2 - Column Logic'''
    print("INFO : PARSE 2 - Column Logic")

    ''' compare rows manually'''
    compare_rows(lhs_only_df, rhs_only_df, final_lhs_only_df, final_rhs_only_df)

    write_diff_to_file(final_lhs_only_df, final_rhs_only_df)

    #print(final_lhs_only_df)
    #print(final_rhs_only_df)

    if(len(final_lhs_only_df) == 0  and len(final_lhs_only_df) == 0):
        print('\n**************************************')
        print('*** RESULT : PASS : P2 : LHS = RHS ***')
        print('**************************************\n')
    else:
        print('\n*************************************')
        print(' RESULT : FAIL ')
        print(' Unmatched LHS : {} '.format(len(final_lhs_only_df)))
        print(' Unmatched RHS : {} '.format(len(final_rhs_only_df)))
        print('*************************************\n')


    print('Exiting... ')
    end_time = time.time()
    print('TIME : Total time taken : {} milliseconds '.format((end_time - start_time) * 1000))
    exit(0)