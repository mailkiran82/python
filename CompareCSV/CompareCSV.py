import pandas as pd
import json
import hashlib
import time
from difflib import SequenceMatcher
import difflib
import numpy as np

''' set display option, only for viewing purpose '''
pd.set_option('display.max_columns', 256)
pd.set_option('display.max_colwidth', 512)
pd.set_option('display.precision', 15)

def my_timer(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print('TIME : {} : {} milliseconds'.format(method.__name__, (te-ts)*1000))
        return result
    return timed

def md5_helper(combinedstr):
    m = hashlib.md5()
    m.update(combinedstr.encode('utf-8'))
    return m.hexdigest()

@my_timer
def pre_check(df,side):
    ''' list all the columns that contains NaN'''
    df_na_columns = df.columns[df.isna().any()].tolist()
    if len(df_na_columns):
        print('PRE-CHECK : TIP : Found NaN in {} file, set default values for these columns \'{}\' in compareconfig'.format(side.upper(),df_na_columns))

@my_timer
def pre_process(config, df,side):
    '''set the default values for NaN'''
    for k, v in config['compareconfig'][side]['fillna'].items():
        df[k].fillna(config['compareconfig']['defaultvalues'][v], inplace=True)
        df[k] = df[k].astype(v)

    ''' list all the columns that contains NaN'''
    df_na_columns = df.columns[df.isna().any()].tolist()
    if len(df_na_columns):
        print('ERROR : These columns have NaN in {} file, set default values for these columns \'{}\' in compareconfig'.format(side,df_na_columns))
        exit(1)

    '''set datatypes as per compareconfig'''
    for k, v in config['compareconfig'][side]['datatype'].items():
        df[k] = df[k].astype(v)

    ''' set timeformat as per compareconfig'''
    for k, v in config['compareconfig'][side]['dateformat'].items():
        df[k] = df[k].dt.strftime(v)
        df[k] = df[k].astype('datetime64')

    ''' set float precision as per compareconfig'''
    for k, v in config['compareconfig'][side]['floatprecision'].items():
        df[k] = df[k].round(v)

    '''set string values to lower'''
    for c in df.select_dtypes(include=[np.dtype(object)]).columns:
        df[c] = df[c].str.lower()

    ''' drop duplicate rows'''
    df.drop_duplicates(inplace=True)


@my_timer
def set_md5_index(df, side):
    ''' add a column with md5, to be used as key'''
    df['__MD5__'] = ''

    ''' join all the cells in the row'''
    for index, row in df.iterrows():
        combinedstr = "~".join(row.astype(str)).lower().replace(' 00:00:00', '')
        df.loc[index, '__MD5__'] = md5_helper(combinedstr)

    validate_md5(df,side)
    df.set_index(keys=df.__MD5__, inplace=True)
    df.drop('__MD5__', axis=1, inplace=True)

def validate_md5(df,side):
    rowcount = len(df)
    md5count = df.__MD5__.nunique()
    if rowcount !=  md5count:
        print("ERROR : Duplicate MD5 found in side {}. row count is {}, MD5 count is {}".format(rowcount, side, md5count))
        exit(1)
    else:
        print("DEBUG : {} MD5 is GOOD. row count is {}, MD5 count is {}".format(side, rowcount, md5count))

@my_timer
def remove_zero_balance_rows(config, df, side):
    ''' remove zero balance rows'''
    if config['compareconfig'][side]['removezerobalancerow'].lower() == "true":
        float_columns = df.select_dtypes(include=[np.float64]).columns.tolist()
        if len(float_columns) > 0:
            float_column_condition = [" (df."+i+" == 0.0) " for i in float_columns]
            condition_string = " & ".join(float_column_condition)
            print("LOG : {} row count BEFORE remove_zero_balance_rows : {} ".format(side.upper(), len(df)))
            df.drop(df[eval(condition_string)].index, inplace=True)
            print("LOG : {} row count AFTER remove_zero_balance_rows : {} ".format(side.upper(), len(df)))

@my_timer
def check_float_tolerance(l_df, r_df):
    common_columns =list(set(l_df.select_dtypes(include=[np.float64]).columns)
                         & set(r_df.select_dtypes(include=[np.float64]).columns))
    print('PRE-CHECK : TIP : set tolerance values for comparision for these float columns \'{}\' in compareconfig'
          .format(common_columns))

@my_timer
def check_string_tolerance(l_df, r_df):
    common_columns = (set(l_df.select_dtypes(include=[np.dtype(object)]).columns)
                         & set(r_df.select_dtypes(include=[np.dtype(object)]).columns))

    print('PRE-CHECK : TIP : set tolerance values for comparision for these string columns \'{}\' in compareconfig'
          .format(common_columns))

    s_tolerance = config['compareconfig']['comparetolerance']['stringtolerance']

    for c in common_columns.difference(set(s_tolerance.keys())):
        ''' length check'''
        l_max_len = len(max(l_df[c], key=len))
        r_max_len = len(max(r_df[c], key=len))

        #print("**",c, " ", max(l_df[c], key=len))
        len_check = True if l_max_len == r_max_len else False

        if len_check == False:
            ''' value check'''
            symmetric_difference = set(set(l_df[c]) ^ set(r_df[c]))

            if len(symmetric_difference):
                print('PRE-CHECK : WARNING : set tolerance values for comparision for this string column \'{}\' in compareconfig'
                    .format(c))

def is_comparable(l_df, r_df):
    match_count = len([i for i, j in zip(l_df.columns.tolist(), r_df.columns.tolist()) if i == j])
    return True if match_count == len(l_df.columns.tolist()) else False

def add_md5_column(df, key_columns):
    ''' add a column with md5, to be used as key'''
    df['__MD5__'] = ''
    ''' join the cells to generate MD5'''
    for index, row in df.iterrows():
        combined_key_str = str('')
        for k, v in row.items():
            if k in key_columns:
                combined_key_str += str(v).lower().replace(' 00:00:00', '')
        df.loc[index, '__MD5__'] = md5_helper(combined_key_str)

@my_timer
def mark_comparable_rows(config, l_df, r_df):
    row_comparability = config['compareconfig']['comparetolerance']['rowcomparability'].lower()
    print("DEBUG : PARSE 2 : row_comparability : {} ".format(row_comparability))

    l_df['__COMBINED_ROW__'] = ''
    l_df['__IS_ROW_COMPARABLE__'] = 0

    r_df['__COMBINED_ROW__'] = ''
    r_df['__IS_ROW_COMPARABLE__'] = 0

    for index, row in l_df.iterrows():
        combined_full_str = "~".join(row.astype(str)).lower().replace(' 00:00:00', '')
        l_df.loc[index, '__COMBINED_ROW__'] = combined_full_str

    for index, row in r_df.iterrows():
        combined_full_str = "~".join(row.astype(str)).lower().replace(' 00:00:00', '')
        r_df.loc[index, '__COMBINED_ROW__'] = combined_full_str

    if row_comparability.lower() == 'low':
        row_comparability_count = 2
    elif row_comparability.lower() == 'medium':
        row_comparability_count = int(l_df.iloc[0]['__COMBINED_ROW__'].count('~')/2)
    else:
        row_comparability_count = int(l_df.iloc[0]['__COMBINED_ROW__'].count('~')/ 1.5)

    print("DEBUG : PARSE 2 : row_comparability_count : {} out of {}".format(row_comparability_count,l_df.iloc[0]['__COMBINED_ROW__'].count('~')))

    for o_index, o_row in l_df.iterrows():
        for i_index, i_row in r_df.iterrows():
            seq_match = SequenceMatcher(None, o_row['__COMBINED_ROW__'], i_row['__COMBINED_ROW__'])
            match = seq_match.find_longest_match(0, len(o_row['__COMBINED_ROW__']), 0, len(i_row['__COMBINED_ROW__']))
            a_start_pos = match[0]
            length = match[2]
            sub_str = str(o_row['__COMBINED_ROW__'])[a_start_pos:a_start_pos+length]
            if sub_str.count('~') >= row_comparability_count:
                l_df.loc[o_index, '__IS_ROW_COMPARABLE__'] = 1
                r_df.loc[i_index, '__IS_ROW_COMPARABLE__'] = 1

@my_timer
def find_key_value_columns(l_df, r_df, k_columns, v_columns):

    for c in l_df.columns.difference(['__MD5__','__COMBINED_ROW__','__IS_ROW_COMPARABLE__']):
        if l_df[c].dtype == np.float64:
            v_columns.append(c)
        elif l_df[c].dtype == np.dtype(object):
            ''' length check, if same length use it as key, else as value'''
            l_max_len = len(max(l_df[c], key=len))
            r_max_len = len(max(r_df[c], key=len))
            len_check = True if l_max_len == r_max_len else False
            if len_check == True:
                k_columns.append(c)
            else:
                v_columns.append(c)
        else:
            k_columns.append(c)

    print("DEBUG : PARSE 2 : key columns : {} ".format(k_columns))
    print("DEBUG : PARSE 2 : value columns : {} ".format(v_columns))

def my_compare_string(a, b, percentage):
    sa = len(a)
    sb = len(b)
    min = sa if sa < sb else sb
    max = sb if sb > sa else sa
    diff = max - min
    l = [li for li in difflib.ndiff(a.lower(), b.lower()) if li[0] != ' ']
    sl = len(l)
    cl = sl - (diff)
    p = int((1 - (cl / min)) * 100)
    return p >= int(percentage)

def generate_row_comparision_stmt(v_columns):
    s_tolerance = config['compareconfig']['comparetolerance']['stringtolerance']
    f_tolerance = config['compareconfig']['comparetolerance']['floattolerance']
    compare_stmt_conditions = []
    for c in v_columns:
        if c in s_tolerance.keys():
            compare_stmt_conditions.append(
                " my_compare_string(o_row." + c + ",i_row." + c + "," + str(s_tolerance[c]) + ") ")
        elif c in f_tolerance.keys():
            compare_stmt_conditions.append(" abs(o_row." + c + " - i_row." + c + ") <= "+str(f_tolerance[c])+" ")
        else:
            compare_stmt_conditions.append(" (o_row." + c + " == i_row." + c + ") ")

    compare_stmt = " and ".join(compare_stmt_conditions)
    return compare_stmt

def l_minus_r_union_r_minus_l(l_df, r_df, f_l_df, f_r_df):
    t_f_l_df = l_df[(~l_df.__MD5__.isin(r_df.__MD5__.values))].copy()
    t_f_r_df = r_df[(~r_df.__MD5__.isin(l_df.__MD5__.values))].copy()

    for index, row in t_f_l_df.iterrows():
        l_df.drop(l_df[l_df['__MD5__'] == row.__MD5__].index, inplace=True)

    for index, row in t_f_r_df.iterrows():
        r_df.drop(r_df[r_df['__MD5__'] == row.__MD5__].index, inplace=True)

    t_f_l_df.drop(['__MD5__'], axis=1, inplace=True)
    t_f_r_df.drop(['__MD5__'], axis=1, inplace=True)

    for index, row in t_f_l_df.iterrows():
        f_l_df.loc[index] = row
        f_l_df.loc[index,'__FILTER__'] = 'from keycheck'

    for index, row in t_f_r_df.iterrows():
        f_r_df.loc[index] = row
        f_r_df.loc[index, '__FILTER__'] = 'from keycheck'

def compare_single_row(compare_stmt, o_df, i_df, l_df, r_df, f_l_df, f_r_df):
    o_row = o_df.iloc[0]
    i_row = i_df.iloc[0]

    flag = eval(compare_stmt)

    if flag:
        ''' Match - delete the row from l_df and r_df'''
        l_df.drop(l_df[l_df.__MD5__ == o_row.__MD5__].index, inplace=True)
        r_df.drop(r_df[r_df.__MD5__ == i_row.__MD5__].index, inplace=True)

    else:
        ''' Not Match - copy to f_l_df and f_r_df'''
        for index, row in o_df.iterrows():
            row.drop(['__MD5__'], inplace=True)
            f_l_df.loc[index] = row
            f_l_df.loc[index, '__FILTER__'] = 'from valuecheck'

        for index, row in i_df.iterrows():
            row.drop(['__MD5__'], inplace=True)
            f_r_df.loc[index] = row
            f_r_df.loc[index, '__FILTER__'] = 'from valuecheck'

        ''' Not Match delete the row from l_df and r_df'''
        l_df.drop(l_df[l_df['__MD5__'] == o_row.__MD5__].index, inplace=True)
        r_df.drop(r_df[r_df['__MD5__'] == i_row.__MD5__].index, inplace=True)

def compare_multiple_row(compare_stmt, o_df, i_df, l_df, r_df, f_l_df, f_r_df):
    o_df['__MATCH_COUNT__'] = 0
    i_df['__MATCH_COUNT__'] = 0

    ''' compare lhs to rhs and get the row match count'''
    for o_index, o_row in o_df.iterrows():
        for i_index, i_row in i_df.iterrows():
            flag = eval(compare_stmt)
            if flag:
                o_row.__MATCH_COUNT__ += 1
                i_row.__MATCH_COUNT__ += 1
                o_df.loc[o_index, '__MATCH_COUNT__'] = o_row.__MATCH_COUNT__
                i_df.loc[i_index, '__MATCH_COUNT__'] = i_row.__MATCH_COUNT__

    ''' check for one to one match'''
    is_one_to_one_mapping = True
    for o_index, o_row in o_df.iterrows():
        if o_row.__MATCH_COUNT__ != 1:
            is_one_to_one_mapping = False
            break

    for i_index, i_row in i_df.iterrows():
        if i_row.__MATCH_COUNT__ != 1:
            is_one_to_one_mapping = False
            break

    if is_one_to_one_mapping:
        ''' Matched - remove from l_df and r_df'''
        for index, row in o_df.iterrows():
            l_df.drop(l_df[l_df['__MD5__'] == row.__MD5__].index, inplace=True)

        for index, row in i_df.iterrows():
            r_df.drop(r_df[r_df['__MD5__'] == row.__MD5__].index, inplace=True)
    else:
        ''' Ambigious - copy to f_l_df and f_r_df'''
        for index, row in o_df.iterrows():
            row.drop(columns=['__MD5__','__MATCH_COUNT__'], inplace=True)
            f_l_df.loc[index] = row
            f_l_df.loc[index, '__FILTER__'] = 'from valuecheck - multiple ambigious rows'

        for index, row in i_df.iterrows():
            row.drop(columns=['__MD5__','__MATCH_COUNT__'], inplace=True)
            f_r_df.loc[index] = row
            f_r_df.loc[index, '__FILTER__'] = 'from valuecheck - multiple ambigious rows'

        ''' Ambigious - delete the rows from l_df and r_df'''
        for index, row in o_df.iterrows():
            l_df.drop(l_df[l_df['__MD5__'] == row.__MD5__].index, inplace=True)

        for index, row in i_df.iterrows():
            r_df.drop(r_df[r_df['__MD5__'] == row.__MD5__].index, inplace=True)

@my_timer
def compare_rows(l_df, r_df, f_l_df, f_r_df):
    key_columns = []
    value_columns = []

    ''' determine key columns and value columns'''
    find_key_value_columns(l_df, r_df, key_columns, value_columns)

    ''' generate md5 and set as column'''
    add_md5_column(l_df, key_columns)
    add_md5_column(r_df, key_columns)

    ''' copy lhs/rhs md5 not in rhs/lhs to final df'''
    l_minus_r_union_r_minus_l(l_df, r_df, f_l_df, f_r_df)

    compare_stmt = generate_row_comparision_stmt(value_columns)
    print(compare_stmt)

    key_union = list(set(l_df.__MD5__.tolist()) | set(r_df.__MD5__.tolist()))

    for k in key_union:
        o_df = l_df[l_df.__MD5__ == k].copy()
        i_df = r_df[r_df.__MD5__ == k].copy()

        if len(o_df) == 1 and len(i_df) == 1:
            ''' one row found for key'''
            compare_single_row(compare_stmt, o_df, i_df, l_df, r_df, f_l_df, f_r_df)
        else:
            ''' multiple row found for key'''
            compare_multiple_row(compare_stmt, o_df, i_df, l_df, r_df, f_l_df, f_r_df)


if __name__ == "__main__":
    start_time = time.time()
    print("IN Main")

    '''read json file'''
    #with open('F1CompareConfig.json') as f:
    with open('CompareConfig_10k.json') as f:
        config = json.load(f)

    ''' read lhs file '''
    #with open('F1_LHS.csv') as lf:
    with open('LHS_10k.csv') as lf:
        lhs_df = pd.read_csv(lf, low_memory=False)

    #with open('F1_RHS.csv') as rf:
    with open('RHS_10k.csv') as rf:
        rhs_df = pd.read_csv(rf, low_memory=False)

    ''' check if files are comparable, it expects the same column names and ordering in both lhs and rhs'''
    if is_comparable(lhs_df, rhs_df) == False:
       print("ERROR : Files are not comparable, check the headers. Requires the same column names and ordering in both lhs and rhs")
       exit(0)

    ''' Starting Phase 1'''
    print("INFO : PARSE 1")

    ''' check for NaN'''
    pre_check(lhs_df, 'lhs')
    pre_check(rhs_df, 'rhs')

    ''' set NaN, datatype, dataframe, floatprecision'''
    print("\nLHS Types Before : \n",lhs_df.dtypes)
    pre_process(config, lhs_df, 'lhs')
    print("LHS Types After : \n", lhs_df.dtypes)
    print("\nRHS Types Before : \n", rhs_df.dtypes)
    pre_process(config, rhs_df, 'rhs')
    print("RHS Types After : \n", rhs_df.dtypes)

    ''' check for float comparision tolerance'''
    if config['compareconfig']['comparetolerance']['findfloattolerancefields'].lower() == "true":
        check_float_tolerance(lhs_df, rhs_df)

    '''check for string comparision tolerance'''
    if config['compareconfig']['comparetolerance']['findstringtolerancefields'].lower() == "true":
        check_string_tolerance(lhs_df, rhs_df)

    '''remove rows with zero balance'''
    remove_zero_balance_rows(config, lhs_df, 'lhs')
    remove_zero_balance_rows(config, rhs_df, 'rhs')

    ''' generate md5 and set as index'''
    set_md5_index(lhs_df, 'lhs')
    set_md5_index(rhs_df, 'rhs')

    ''' get rows only in lhs'''
    lhs_only_df = lhs_df[(~lhs_df.index.isin(rhs_df.index.values))].copy()

    ''' get rows only in rhs'''
    rhs_only_df = rhs_df[(~rhs_df.index.isin(lhs_df.index.values))].copy()

    if len(lhs_only_df) ==0 & len(rhs_only_df) == 0:
        print('\n**************************************')
        print('*** RESULT : PASS : P1 : LHS = RHS ***')
        print('**************************************\n')
        print('Exiting... ')
        end_time = time.time()
        print('TIME : Total time taken : {} milliseconds '.format((end_time - start_time) * 1000))
        exit(0)

    ''' Starting Phase 2 - Row Logic'''
    print("INFO : PARSE 2 - Row Logic")

    print("\nlhs : \n", lhs_only_df.dtypes)
    print(lhs_only_df)
    print('lhs only row count : {}', len(lhs_only_df))

    print("\nrhs : \n", rhs_only_df.dtypes)
    print(rhs_only_df)
    print('rhs only row count : {}', len(rhs_only_df))

    mark_comparable_rows(config, lhs_only_df, rhs_only_df)

    ''' copy the non comparable rows to final df'''
    final_lhs_only_df = lhs_only_df[lhs_only_df['__IS_ROW_COMPARABLE__'] == 0].copy()
    final_rhs_only_df = rhs_only_df[rhs_only_df['__IS_ROW_COMPARABLE__'] == 0].copy()
    ''' drop columns'''
    final_lhs_only_df.drop(['__IS_ROW_COMPARABLE__','__COMBINED_ROW__'], axis=1, inplace=True)
    final_rhs_only_df.drop(['__IS_ROW_COMPARABLE__', '__COMBINED_ROW__'], axis=1, inplace=True)
    ''' add filter'''
    final_lhs_only_df['__FILTER__']='from rowcomparability'
    final_rhs_only_df['__FILTER__'] = 'from rowcomparability'

    '''drop the non comparable rows in working copy'''
    lhs_only_df.drop(lhs_only_df[lhs_only_df['__IS_ROW_COMPARABLE__'] == 0].index, inplace=True)
    rhs_only_df.drop(rhs_only_df[rhs_only_df['__IS_ROW_COMPARABLE__'] == 0].index, inplace=True)
    '''drop columns'''
    lhs_only_df.drop(['__IS_ROW_COMPARABLE__','__COMBINED_ROW__'], axis=1, inplace=True)
    rhs_only_df.drop(['__IS_ROW_COMPARABLE__', '__COMBINED_ROW__'], axis=1, inplace=True)

    ''' Starting Parse 2 - Column Logic'''
    print("INFO : PARSE 2 - Column Logic")

    ''' compare rows manually'''
    compare_rows(lhs_only_df, rhs_only_df, final_lhs_only_df, final_rhs_only_df)


    final_lhs_only_df.to_csv('Unmatched_F1_LHS.csv', index=False)
    final_rhs_only_df.to_csv('Unmatched_F1_RHS.csv', index=False)

    if(len(final_lhs_only_df) == 0  and len(final_lhs_only_df) == 0):
        print('\n**************************************')
        print('*** RESULT : PASS : P2 : LHS = RHS ***')
        print('**************************************\n')
    else:
        print('\n*************************************')
        print(' RESULT : FAIL ')
        print(' Unmatched LHS : {} '.format(len(final_lhs_only_df)))
        print(' Unmatched RHS : {} '.format(len(final_rhs_only_df)))
        print('*************************************\n')

    print('Exiting... ')
    end_time = time.time()
    print('TIME : Total time taken : {} milliseconds '.format((end_time - start_time) * 1000))
    exit(0)
